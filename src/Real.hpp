#ifndef __zppdb__Real__
#define __zppdb__Real__

#include <iostream>
#include "Field.hpp"
#include <stdexcept>

namespace zppdb {

class Real : public Field
{
public:
	Real() :
		Field(true)
	{
	}
	
	Real(std::nullptr_t const & nullPtr) :
		Field(true)
	{
	}
	
	explicit Real(double const & value) :
		Field(false), _value(value)
	{
	}
	
	Real & operator=(double const & value)
	{
		setRealValue(value);
		return *this;
	}
	
	double value() const
	{
		if(Field::isNull()) {
			throw std::runtime_error("Cannot retrieve value when null.");
		}
		return _value;
	}
	
	virtual Type type() const
	{
		return Type::REAL;
	}
	
	virtual int64_t integerValue() const
	{
		throw std::runtime_error("Cannot retrieve integerValue() from Real.");
	}
	
	virtual double realValue() const
	{
		return value();
	}
	
	virtual std::string textValue() const
	{
		throw std::runtime_error("Cannot retrieve textValue() from Real.");
	}
	
	virtual void setIntegerValue(int64_t value)
	{
		throw std::runtime_error("Real cannot set integer value");
	}
	
	virtual void setRealValue(double value)
	{
		setNull(false);
		_value = value;
	}
	
	virtual void setTextValue(std::string const & value)
	{
		throw std::runtime_error("Real cannot set string value");
	}
private:
	double _value;
};

} // namespace zppdb

#endif /* defined(__zppdb__Real__) */
