#include "Database.hpp"
#include <unistd.h>
#include <cmath>
#include <iostream>
#include <stdexcept>

using namespace std;

namespace zppdb {

Database::Database()
{
	sqliteDb = NULL;
}

Database::Database(std::string const & filename) {
	if(filename == "") {
		sqliteDb = NULL;
	}
	else {
		if(sqlite3_open(filename.c_str(), &sqliteDb) != SQLITE_OK) {
			throw std::runtime_error(string(sqlite3_errmsg(sqliteDb)));
		}
	}
}

Database::~Database() {
	if(sqliteDb != NULL) {
		for(auto & kv : _insertStatements) {
			sqlite3_finalize(kv.second);
		}
		for(auto & kv : _queryStatements) {
			sqlite3_finalize(kv.second);
		}
		
		if(sqlite3_close(sqliteDb) != SQLITE_OK) {
			throw std::runtime_error(string(sqlite3_errmsg(sqliteDb)));
		}
	}
}

void Database::beginTransaction() const {
	if(sqliteDb == NULL) {
		return;
	}
	
	execute("BEGIN TRANSACTION");
}

bool Database::commit() const {
	if(sqliteDb == NULL) {
		return true;
	}
	
	int result = sqlite3_exec(sqliteDb, "COMMIT", NULL, NULL, NULL);
	if(result == SQLITE_BUSY) {
		return false;
	}
	else if(result != SQLITE_OK) {
		throw std::runtime_error(string(sqlite3_errmsg(sqliteDb)));
	}
	return true;
}

bool Database::commitWithRetry(double retryDelay, size_t maxRetries, std::ostream & errStream) const
{
	if(sqliteDb == NULL) {
		return true;
	}
	
	if(retryDelay <= 0.0) {
		throw std::runtime_error("Retry delay must be a positive number.");
	}
	
	useconds_t retryDelayUsecs = (useconds_t)ceil(retryDelay * 1000000);
	bool done = false;
	for(size_t i = 0; !done && i <= maxRetries; i++) {
		done = commit();
		if(!done) {
			usleep(retryDelayUsecs);
			errStream << "Commit unsuccessful due to busy database; retrying in " << retryDelay << "s" << '\n';
		}
	}
	return done;
}

void Database::execute(std::string const & sqlStr) const
{
	if(sqliteDb == NULL) {
		return;
	}
	
	int result = sqlite3_exec(sqliteDb, sqlStr.c_str(), NULL, NULL, NULL);
	if(result) {
		throw std::runtime_error(string(sqlite3_errmsg(sqliteDb)));
	}
}

} // namespace zppdb
