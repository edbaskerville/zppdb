#ifndef __zppdb__Text__
#define __zppdb__Text__

#include "Field.hpp"
#include <string>
#include <stdexcept>

namespace zppdb {

class Text : public Field
{
public:
	Text() :
		Field(true)
	{
	}
	
	Text(std::nullptr_t const & nullPtr) :
		Field(true)
	{
	}
	
	explicit Text(std::string const & text) :
		Field(false), _value(text)
	{
	}
	
	Text & operator=(std::string const & text)
	{
		setTextValue(text);
		return *this;
	}
	
	std::string value() const
	{
		if(Field::isNull()) {
			throw std::runtime_error("Cannot retrieve value when null.");
		}
		return _value;
	}
	
	operator std::string() const
	{
		return value();
	}
	
	virtual Type type() const
	{
		return Type::TEXT;
	}
	
	virtual int64_t integerValue() const
	{
		throw std::runtime_error("Cannot retrieve integerValue() from Text.");
	}
	
	virtual double realValue() const
	{
		throw std::runtime_error("Cannot retrieve integerValue() from Text.");
	}
	
	virtual std::string textValue() const
	{
		return value();
	}
	
	virtual void setIntegerValue(int64_t value)
	{
		throw std::runtime_error("Text cannot set integer value");
	}
	
	virtual void setRealValue(double value)
	{
		throw std::runtime_error("Text cannot set real value");
	}
	
	virtual void setTextValue(std::string const & value)
	{
		setNull(false);
		_value = value;
	}
private:
	std::string _value;
};

} // namespace zppdb

#endif /* defined(__zppdb__Text__) */
