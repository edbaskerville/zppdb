#ifndef __zppdb__Database__
#define __zppdb__Database__

#include <string>
#include <unordered_map>
#include <vector>
#include <sqlite3.h>
#include <stdexcept>
#include "Table.hpp"
#include "Text.hpp"
#include "Integer.hpp"
#include "Real.hpp"
#include "Query.hpp"

namespace zppdb {

/**
	@class Database
	@brief Representation of SQLite3 database.
	
	This class follows RAII (resource allocation is initialization). The SQLite
	database connection is opened in the constructor, and the connection is
	closed, and resources released, in the destructor.
*/
class Database
{
public:
	/**
		@function Database
		@brief Construct no-op database.
		
		This constructor creates an object with no underlying SQLite3 database.
		Operations on this database do nothing; it can be used to test code.
	*/
	Database();
	
	/**
		@function Database
		@brief Constructor for SQLite database
		@throw `std::runtime_error` with the SQLite error message in the event
		of failure.
		
		If `filename` is the empty string, then this constructor behaves the
		same as the no-argument constructor and creates a no-op database.
		
		Otherwise, this constructor creates a SQLite3 database at `filename`.
		This string is passed directly to SQLite, including, e.g., `":memory:"`
		to create an in-memory database. The constructor
	*/
	Database(std::string const & filename);
	
	/**
		@function ~Database
		@brief Destructor.
		@throw `std::runtime_error` with the SQLite error message in the event
		of failure.
		
		Closes SQLite database connection, releasing resources.
	*/
	~Database();
	
	/**
		@function beginTransaction
		@brief Begins a database transaction.
		@throw `std::runtime_error` with the SQLite error message in the event
		of failure.
		
		Note that transactions are not required in SQLite but may need to be
		used if database I/O becomes a performance bottleneck. If so, operations
		need to be enclosed in paired `beginTransaction` and `commit`
		or `commitWithRetry` calls.
	*/
	void beginTransaction() const;
	
	/**
		@function commit
		@brief Commits a database transaction.
		@return `true` if commit was successful; `false` if not.
	*/
	bool commit() const;
	
	/**
		@function commitWithRetry
		@brief Commits a database transaction with retries in the event of
		failure.
		
		This function performs a databse commit, and, if unsuccessful, retries
		again up to `maxRetries` times with a `retryDelay`-second delay between
		each retry, rounded up to the nearest microsecond.
	*/
	bool commitWithRetry(double retryDelay, size_t maxRetries, std::ostream & errStream) const;
	
	/**
		@function execute
		@brief Executes a SQL query.
		@throw `std::runtime_error` with the SQLite error message in the event
		of failure.
		
		This function executes a single-string SQL query.
	*/
	void execute(std::string const & sqlStr) const;
	
	/**
		@function createTable
		@brief Creates a table.
	*/
	template<typename R>
	void createTable(Table<R> const & table)
	{
		createTable(table, false);
	}
	
	/**
		@function createTableIfNotExists
		@brief Creates a table if it doesn't exist.
	*/
	template<typename R>
	void createTableIfNotExists(Table<R> const & table)
	{
		createTable(table, true);
	}
	
	/**
		@function tableExists
		@brief Checks if a table exists
		@returns true if table exists, false otherwise.
	*/
	template<typename R>
	bool tableExists(Table<R> const & table)
	{
		Query query(
			"SELECT name FROM sqlite_master WHERE type = 'table' AND name = '"
			+ table.name + "'"
		);
		int result = step(query);
		finalize(query);
		
		return result == SQLITE_ROW;
	}
	
	/**
		@function createTable
		@brief Creates a table.
	*/
	template<typename R>
	void createTable(Table<R> const & table, bool ifNotExists)
	{
		if(sqliteDb == NULL) {
			return;
		}
		
		std::string tableName = table.name;
		R prototype;
		std::string queryStr = "CREATE TABLE ";
		if(ifNotExists) {
			queryStr += "IF NOT EXISTS ";
		}
		
		queryStr += tableName + " (";
		for(size_t i = 0; i < prototype.columnCount(); i++) {
			queryStr += prototype.field(i).name;
			switch(prototype.field(i).ptr->type()) {
				case Type::INTEGER:
					queryStr += " INTEGER";
					break;
				case Type::REAL:
					queryStr += " REAL";
					break;
				case Type::TEXT:
					queryStr += " TEXT";
					break;
			}
			if(i < prototype.columnCount() - 1) {
				queryStr += ", ";
			}
		}
		queryStr += ");";
		
		std::cerr << queryStr << std::endl;
		execute(queryStr);
	}
	
	/**
		@function insert
		@brief Inserts a row into a table.
	*/
	template<typename R>
	void insert(Table<R> const & table, R const & row)
	{
		if(sqliteDb == NULL) {
			return;
		}
		
		sqlite3_stmt * ppStmt = getInsertStatement(table);
		for(int64_t i = 0; i < row.columnCount(); i++) {
			Field const * fieldPtr = row.field(i).ptr;
			int result;
			
			if(fieldPtr->isNull()) {
				result = sqlite3_bind_null(ppStmt, int(i+1));
			}
			else {
				switch(fieldPtr->type()) {
					case Type::INTEGER:
						result = sqlite3_bind_int64(ppStmt, int(i+1), fieldPtr->integerValue());
						break;
					case Type::REAL:
						result = sqlite3_bind_double(ppStmt, int(i+1), fieldPtr->realValue());
						break;
					case Type::TEXT:
						{
							std::string textStr = fieldPtr->textValue();
							char const * text = textStr.c_str();
							result = sqlite3_bind_text(ppStmt, int(i+1), text, -1, SQLITE_TRANSIENT);
						}
						break;
				}
			}
			
			if(result != SQLITE_OK) {
				throw std::runtime_error(std::string(sqlite3_errmsg(sqliteDb)));
			}
		}
		
		// Execute prepared statement
		int result = sqlite3_step(ppStmt);
		if(result != SQLITE_DONE) {
			throw std::runtime_error(std::string(sqlite3_errmsg(sqliteDb)));
		}
		
		// Reset prepared statement for next step
		result = sqlite3_reset(ppStmt);
		if(result != SQLITE_OK) {
			throw std::runtime_error(std::string(sqlite3_errmsg(sqliteDb)));
		}
	}
	
	template<typename R>
	std::vector<R> readTable(Table<R> const & table)
	{
        return readTable(table, "");
	}
	
	template<typename R>
	std::vector<R> readTable(Table<R> const & table, std::string const & sqlWhereStr)
	{
		std::vector<R> rows;
		
		// Construct query string
		std::string sqlStr = "SELECT * FROM ";
		sqlStr += table.name;
        sqlStr += " ";
        sqlStr += sqlWhereStr;
		sqlStr += ";";
		char const * sqlCStr = sqlStr.c_str();
		
		// Create prepared statement
		sqlite3_stmt * ppStmt;
		int result = sqlite3_prepare_v2(sqliteDb, sqlCStr, -1, &ppStmt, NULL);
		if(result) {
			throw std::runtime_error(std::string(sqlite3_errmsg(sqliteDb)));
		}
		
		bool done = false;
		while(!done) {
			result = sqlite3_step(ppStmt);
			if(result == SQLITE_ROW) {
				rows.emplace_back();
				if(sqlite3_column_count(ppStmt) != rows.back().columnCount())
				{
					sqlite3_finalize(ppStmt);
					throw std::runtime_error("Column count of SELECT * does not match row class.");
				}
				for(int i = 0; i < rows.back().columnCount(); i++) {
					if(sqlite3_column_type(ppStmt, i) != SQLITE_NULL) {
						Field * fieldPtr = rows.back().field(i).ptr;
						switch(fieldPtr->type()) {
							case Type::INTEGER:
								fieldPtr->setIntegerValue(sqlite3_column_int64(ppStmt, i));
								break;
							case Type::REAL:
								fieldPtr->setRealValue(sqlite3_column_double(ppStmt, i));
								break;
							case Type::TEXT:
								fieldPtr->setTextValue(reinterpret_cast<char const *>(
									sqlite3_column_text(ppStmt, i)
								));
								break;
							default:
								sqlite3_finalize(ppStmt);
								throw std::runtime_error("Unsupported type found for column");
						}
					}
				}
			}
			else if(result == SQLITE_DONE){
				done = true;
			}
			else {
				sqlite3_finalize(ppStmt);
				throw std::runtime_error(sqlite3_errmsg(sqliteDb));
			}
		}
		
		// Finalize prepared statement
		result = sqlite3_finalize(ppStmt);
		if(result) {
			throw std::runtime_error(std::string(sqlite3_errmsg(sqliteDb)));
		}
		
		return rows;
	}
	
	int bindNull(Query const & query, int column)
	{
		return sqlite3_bind_null(getQueryStatement(query), int(column+1));
	}
	
	int bindInteger(Query const & query, int column, int64_t value)
	{
		return sqlite3_bind_int64(getQueryStatement(query), int(column+1), value);
	}
	
	int bindReal(Query const & query, int column, double value)
	{
		return sqlite3_bind_double(getQueryStatement(query), int(column+1), value);
	}
	
	int bindText(Query const & query, int column, std::string const & value)
	{
		return sqlite3_bind_text(getQueryStatement(query), int(column+1), value.c_str(), -1, SQLITE_TRANSIENT);
	}
	
	int step(Query const & query)
	{
		return sqlite3_step(getQueryStatement(query));
	}
	
	int reset(Query const & query)
	{
		return sqlite3_reset(getQueryStatement(query));
	}
	
	int finalize(Query const & query)
	{
		if(_queryStatements.find(&query) == _queryStatements.end()) {
			return SQLITE_OK;
		}
		sqlite3_stmt * ppStmt = _queryStatements[&query];
		_queryStatements.erase(&query);
		return sqlite3_finalize(ppStmt);
	}
	
	bool isNull(Query const & query, int column)
	{
		return sqlite3_column_type(getQueryStatement(query), column) == SQLITE_NULL;
	}
	
	int64_t getInteger(Query const & query, int column)
	{
		return sqlite3_column_int64(getQueryStatement(query), column);
	}
	
	double getReal(Query const & query, int column)
	{
		return sqlite3_column_double(getQueryStatement(query), column);
	}
	
	std::string getText(Query const & query, int column)
	{
		return std::string(reinterpret_cast<char const *>(
			sqlite3_column_text(getQueryStatement(query), column)
		));
	}
	
private:
	sqlite3 * sqliteDb;
	std::unordered_map<void *, sqlite3_stmt *> _insertStatements;
	std::unordered_map<Query const *, sqlite3_stmt *> _queryStatements;
	
	template<typename R>
	sqlite3_stmt * getInsertStatement(Table<R> const & table)
	{
		R prototype;
		
		std::string tableName = table.name;
		auto itr = _insertStatements.find((void *)&table);
		sqlite3_stmt * ppStmt;
		if(itr == _insertStatements.end()) {
			// Construct SQL insert statement with ? parameters
			std::string queryStr = "INSERT INTO " + tableName + " VALUES (";
			for(int64_t i = 0; i < prototype.columnCount(); i++) {
				queryStr += "?";
				if(i < prototype.columnCount() - 1) {
					queryStr += ", ";
				}
			}
			queryStr += ");";
			char const * queryCStr = queryStr.c_str();
			
			// Create prepared statement
			int result = sqlite3_prepare_v2(sqliteDb, queryCStr, -1, &ppStmt, NULL);
			if(result) {
				throw std::runtime_error(std::string(sqlite3_errmsg(sqliteDb)));
			}
			
			_insertStatements[(void *)&table] = ppStmt;
		}
		else {
			ppStmt = itr->second;
		}
		return ppStmt;
	}
	
	sqlite3_stmt * getQueryStatement(Query const & query)
	{
		auto itr = _queryStatements.find(&query);
		sqlite3_stmt * ppStmt;
		if(itr == _queryStatements.end()) {
			int result = sqlite3_prepare_v2(sqliteDb, query.queryString.c_str(), -1, &ppStmt, NULL);
			if(result) {
				throw std::runtime_error(sqlite3_errmsg(sqliteDb));
			}
			_queryStatements[&query] = ppStmt;
		}
		else {
			ppStmt = itr->second;
		}
		return ppStmt;
	}
};


} // namespace zppdb

#endif /* defined(__zppdb__Database__) */
