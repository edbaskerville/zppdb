#ifndef __zppdb__Field__
#define __zppdb__Field__

#include <string>
#include <inttypes.h>

namespace zppdb {

enum class Type
{
	INTEGER,
	REAL,
	TEXT
};

class Field
{
public:
	Field(bool isNull) :
		_isNull(isNull)
	{
	}
	
	void setNull(bool isNull)
	{
		_isNull = isNull;
	}
	
	void setNull()
	{
		_isNull = true;
	}
	
	bool isNull() const
	{
		return _isNull;
	}
	
	virtual Type type() const = 0;
	
	virtual int64_t integerValue() const = 0;
	virtual double realValue() const = 0;
	virtual std::string textValue() const = 0;
	
	virtual void setIntegerValue(int64_t value) = 0;
	virtual void setRealValue(double value) = 0;
	virtual void setTextValue(std::string const & value) = 0;
protected:
	bool _isNull;
};

} // namespace zppdb

#endif /* defined(__zppdb__Field__) */
