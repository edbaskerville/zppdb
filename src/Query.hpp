class Query
{
public:
	Query(std::string const & queryString) : queryString(queryString) {};
	std::string const queryString;
};
