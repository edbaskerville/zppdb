#ifndef __zppdb_macros__
#define __zppdb_macros__

#include "Field.hpp"
#include <vector>
#include <string>
#include <boost/preprocessor.hpp>

namespace zppdb
{

struct FieldInfo
{
	FieldInfo(std::string const & name, Field * ptr) :
		name(name), ptr(ptr)
	{
	}
	
	std::string const name;
	Field * ptr;
};

}

#define ZPPDB_DEFINE_COLUMN(r, data, typeSpec) \
	BOOST_PP_SEQ_ELEM(0, typeSpec) BOOST_PP_SEQ_ELEM(1, typeSpec);

#define ZPPDB_APPEND_FIELD(r, fields, typeSpec) \
	fields.emplace_back( \
		BOOST_PP_STRINGIZE(BOOST_PP_SEQ_ELEM(1, typeSpec)), \
		&BOOST_PP_SEQ_ELEM(1, typeSpec) \
	);

#define ZPPDB_DEFINE_ROW_TYPE(dbTypeName, typeSeq) \
class dbTypeName { \
public: \
	BOOST_PP_SEQ_FOR_EACH(ZPPDB_DEFINE_COLUMN, data, typeSeq) \
	\
	dbTypeName(): _fields(_makeFields()) {} \
	\
	std::vector<zppdb::FieldInfo> const & fields() const { return _fields; } \
	\
	FieldInfo const & field(size_t index) const { return _fields[index]; } \
	\
	size_t columnCount() const \
	{ \
		return _fields.size(); \
	} \
private: \
	std::vector<zppdb::FieldInfo> const _fields; \
	\
	std::vector<zppdb::FieldInfo> _makeFields() \
	{ \
		std::vector<zppdb::FieldInfo> fields; \
		BOOST_PP_SEQ_FOR_EACH(ZPPDB_APPEND_FIELD, fields, typeSeq) \
		return fields; \
	} \
};

#endif
