#ifndef __zppdb__DBTable__
#define __zppdb__DBTable__

#include <string>

namespace zppdb {

template<typename R>
class Table
{
public:
	Table(std::string const & name) :
		name(name)
	{
	}
	
	std::string const name;
};

} // namespace zppdb

#endif /* defined(__zppdb__DBTable__) */
