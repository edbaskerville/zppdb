#ifndef __zppdb__Integer__
#define __zppdb__Integer__

#include "Field.hpp"
#include <iostream>
#include <stdexcept>

namespace zppdb {

class Integer : public Field
{
public:
	Integer() :
		Field(true)
	{
	}
	
	Integer(std::nullptr_t const & nullPtr) :
		Field(true)
	{
	}
	
	explicit Integer(int64_t const & value) :
		Field(false), _value(value)
	{
	}
	
	Integer & operator=(int64_t const & value)
	{
		setIntegerValue(value);
		return *this;
	}
	
	int64_t value() const
	{
		if(Field::isNull()) {
			throw std::runtime_error("Cannot retrieve value when null.");
		}
		return _value;
	}
	
	virtual Type type() const
	{
		return Type::INTEGER;
	}
	
	virtual int64_t integerValue() const
	{
		return value();
	}
	
	virtual double realValue() const
	{
		throw std::runtime_error("Cannot retrieve realValue() from Integer.");
	}
	
	virtual std::string textValue() const
	{
		throw std::runtime_error("Cannot retrieve textValue() from Integer.");
	}
	
	virtual void setIntegerValue(int64_t value)
	{
		setNull(false);
		_value = value;
	}
	
	virtual void setRealValue(double value)
	{
		throw std::runtime_error("Integer cannot set real value");
	}
	
	virtual void setTextValue(std::string const & value)
	{
		throw std::runtime_error("Integer cannot set string value");
	}
private:
	int64_t _value;
};

} // namespace zppdb

#endif /* defined(__zppdb__Integer__) */
