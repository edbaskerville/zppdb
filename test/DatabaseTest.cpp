#include "catch.hpp"
#include "zppdb.hpp"
#include <iostream>
#include <vector>

using namespace zppdb;
using namespace std;

ZPPDB_DEFINE_ROW_TYPE(MyRow,
	( (Real)(a) )
	( (Integer)(b) )
	( (Text)(c) )
	( (Real)(d) )
	( (Integer)(e) )
	( (Text)(f) )
)

TEST_CASE("Exists test", "[Database]")
{
	Database db(":memory:");
	Table<MyRow> table("supertable");
	REQUIRE(!db.tableExists(table));
	db.createTable(table);
	REQUIRE(db.tableExists(table));
}

TEST_CASE("Database test", "[Database]")
{
	Database db("test.sqlite3");
	
	MyRow row;
	REQUIRE(row.a.isNull());
	row.a = 5.0;
	REQUIRE(!row.a.isNull());
	REQUIRE(row.a.value() == 5.0);
	
	REQUIRE(row.b.isNull());
	row.b = 100;
	REQUIRE(!row.b.isNull());
	REQUIRE(row.b.value() == 100);
	
	REQUIRE(row.c.isNull());
	
	REQUIRE(row.d.isNull());
	row.d = 4.5;
	REQUIRE(!row.d.isNull());
	REQUIRE(row.d.value() == 4.5);
	
	REQUIRE(row.f.isNull());
	row.f = "hello";
	REQUIRE(!row.f.isNull());
	REQUIRE(row.f.value() == "hello");
	
	Table<MyRow> table("blah");
	db.createTableIfNotExists(table);
	db.insert(table, row);
	
	vector<MyRow> rows = db.readTable(table);
	for(MyRow & row : rows) {
		cerr << row.a.realValue() << endl;
		cerr << row.b.integerValue() << endl;
		REQUIRE_THROWS(cerr << row.c.textValue() << endl);
		cerr << row.d.realValue() << endl;
		REQUIRE_THROWS(cerr << row.e.integerValue() << endl);
		cerr << row.f.textValue() << endl;
	}
}

