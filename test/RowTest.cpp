#include "catch.hpp"
#include "Database.hpp"

using namespace std;

#include <iostream>
#include "zppdb.hpp"

using namespace zppdb;

ZPPDB_DEFINE_ROW_TYPE(MyRow,
	( (Real)(a) )
	( (Integer)(b) )
	( (Text)(c) )
	( (Real)(d) )
	( (Integer)(e) )
	( (Text)(f) )
)

TEST_CASE("Row test", "[Row]")
{
	MyRow row;
	REQUIRE(row.a.isNull());
	row.a = 5.0;
	REQUIRE(!row.a.isNull());
	REQUIRE(row.a.value() == 5.0);
	
	REQUIRE(row.f.isNull());
	row.f = "hello";
	REQUIRE(!row.f.isNull());
	REQUIRE(row.f.value() == "hello");
}

